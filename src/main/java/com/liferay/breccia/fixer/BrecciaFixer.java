/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.breccia.fixer;

import java.io.IOException;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.UnaryOperator;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Andrea Di Giorgi
 */
public class BrecciaFixer {

	public static void main(String[] args) throws IOException {
		new BrecciaFixer(Paths.get(args[0]));
	}

	public BrecciaFixer(Path path) throws IOException {
		JSONArray jsonArray = _readJSONArray(path);

		for (int i = 0; i < jsonArray.length(); i++) {
			_processProduct(jsonArray.getJSONObject(i));
		}

		Path dirPath = path.getParent();

		dirPath = dirPath.resolve("new");

		Files.createDirectories(dirPath);

		_writeJSONArray(
			_optionCategoryJSONObjects.values(),
			dirPath.resolve("option-categories.json"));
		_writeJSONArray(
			_optionJSONObjects.values(), dirPath.resolve("options.json"));
		_writeJSONArray(
			_specificationOptionJSONObjects.values(),
			dirPath.resolve("specification-options.json"));

		_writeJSONArray(
			_productJSONObjects.values(), dirPath.resolve(path.getFileName()));
	}

	private static <T> void _appendAll(
		Collection<T> collection, Iterable<T> values) {

		for (T value : values) {
			collection.add(value);
		}
	}

	private static <T> T _copy(
		JSONObject sourceJSONObject, JSONObject destinationJSONObject,
		String key) {

		return _copy(
			sourceJSONObject, destinationJSONObject, key,
			UnaryOperator.<T>identity());
	}

	private static Boolean _copy(
		JSONObject sourceJSONObject, JSONObject destinationJSONObject,
		String key, boolean defaultValue) {

		Boolean value = _get(sourceJSONObject, key);

		if ((value == null) || (value.booleanValue() == defaultValue)) {
			return value;
		}

		destinationJSONObject.put(key, value.booleanValue());

		return value;
	}

	private static <T> T _copy(
		JSONObject sourceJSONObject, JSONObject destinationJSONObject,
		String key, UnaryOperator<T> transformer) {

		T value = _get(sourceJSONObject, key);

		if (value == null) {
			return null;
		}

		value = transformer.apply(value);

		if (value != null) {
			destinationJSONObject.put(key, value);
		}

		return value;
	}

	private static <T> T _get(JSONObject jsonObject, String key) {
		T value = null;

		if (jsonObject.has(key)) {
			value = (T)jsonObject.get(key);
		}
		else {
			if (key.startsWith("DDM")) {
				key = "ddm" + key.substring(3);
			}
			else {
				key = Character.toLowerCase(key.charAt(0)) + key.substring(1);
			}

			if (jsonObject.has(key)) {
				value = (T)jsonObject.get(key);
			}
		}

		return value;
	}

	private static JSONArray _getJSONArray(Iterable<?> values) {
		JSONArray jsonArray = new JSONArray();

		for (Object value : values) {
			jsonArray.put(value);
		}

		return jsonArray;
	}

	private static JSONArray _merge(
		JSONArray jsonArray1, JSONArray jsonArray2) {

		Set<Object> values = new TreeSet<>();

		_appendAll(values, jsonArray1);
		_appendAll(values, jsonArray2);

		return _getJSONArray(values);
	}

	private static JSONArray _readJSONArray(Path path) throws IOException {
		String json = new String(
			Files.readAllBytes(path), StandardCharsets.UTF_8);

		return new JSONArray(json);
	}

	private static void _writeJSONArray(
			Iterable<JSONObject> jsonObjects, Path path)
		throws IOException {

		JSONArray jsonArray = _getJSONArray(jsonObjects);

		String json = jsonArray.toString(4);

		json = json.replace("    ", "\t");

		Files.write(path, json.getBytes(StandardCharsets.UTF_8));
	}

	private JSONObject _processOptionCategory(JSONObject jsonObject) {
		JSONObject optionCategoryJSONObject = new JSONObject();

		String key = _copy(
			jsonObject, optionCategoryJSONObject, "Key", String::toLowerCase);

		_optionCategoryJSONObjects.put(key, optionCategoryJSONObject);

		return optionCategoryJSONObject;
	}

	private JSONArray _processOptions(JSONArray jsonArray) {
		Map<String, JSONObject> productOptionJSONObjects = new TreeMap<>();

		for (int i = 0; i < jsonArray.length(); i++) {

			// Product

			JSONObject jsonObject = jsonArray.getJSONObject(i);

			JSONObject productOptionJSONObject = new JSONObject();

			String key = _copy(
				jsonObject, productOptionJSONObject, "Key",
				String::toLowerCase);

			JSONArray valuesJSONArray = _get(jsonObject, "Values");

			JSONArray productValuesJSONArray = new JSONArray();

			for (int j = 0; j < valuesJSONArray.length(); j++) {
				String valueKey = null;

				JSONObject valueJSONObject = valuesJSONArray.optJSONObject(j);

				if (valueJSONObject == null) {
					valueKey = valuesJSONArray.getString(j);
				}
				else {
					valueKey = _get(valueJSONObject, "Key");
				}

				productValuesJSONArray.put(valueKey.toLowerCase());
			}

			productOptionJSONObject.put("Values", productValuesJSONArray);

			productOptionJSONObjects.put(key, productOptionJSONObject);

			// All

			JSONObject optionJSONObject = new JSONObject();

			_copy(jsonObject, optionJSONObject, "DDMFormFieldTypeName");

			key = _copy(
				jsonObject, optionJSONObject, "Key", String::toLowerCase);

			_copy(jsonObject, optionJSONObject, "Required", false);
			_copy(jsonObject, optionJSONObject, "SkuContributor", false);

			optionJSONObject.put("Values", productValuesJSONArray);

			JSONObject existingOptionJSONObject = _optionJSONObjects.get(key);

			if (existingOptionJSONObject != null) {
				JSONArray existingValuesJSONArray =
					existingOptionJSONObject.getJSONArray("Values");

				existingOptionJSONObject.put(
					"Values",
					_merge(existingValuesJSONArray, productValuesJSONArray));
			}
			else {
				_optionJSONObjects.put(key, optionJSONObject);
			}
		}

		return _getJSONArray(productOptionJSONObjects.values());
	}

	private JSONObject _processProduct(JSONObject jsonObject) {
		JSONObject productJSONObject = new JSONObject();

		_copy(jsonObject, productJSONObject, "Categories");
		_copy(jsonObject, productJSONObject, "Description");
		_copy(jsonObject, productJSONObject, "Images");

		String name = _copy(jsonObject, productJSONObject, "Name");

		_copy(
			jsonObject, productJSONObject, "Price",
			price -> Double.valueOf((String)price));

		_copy(jsonObject, productJSONObject, "ShortDescription");
		_copy(jsonObject, productJSONObject, "Sku");

		JSONArray optionCategoriesJSONArray = _get(
			jsonObject, "OptionCategories");

		if (optionCategoriesJSONArray != null) {
			for (int i = 0; i < optionCategoriesJSONArray.length(); i++) {
				_processOptionCategory(
					optionCategoriesJSONArray.getJSONObject(i));
			}
		}

		_copy(jsonObject, productJSONObject, "Options", this::_processOptions);

		_copy(
			jsonObject, productJSONObject, "SpecificationOptions",
			this::_processSpecificationOptions);

		_productJSONObjects.put(name, productJSONObject);

		return productJSONObject;
	}

	private JSONArray _processSpecificationOptions(JSONArray jsonArray) {
		Map<String, JSONObject> productSpecificationOptionJSONObjects =
			new TreeMap<>();

		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);

			// Product

			JSONObject productSpecificationOptionJSONObject = new JSONObject();

			String key = _copy(
				jsonObject, productSpecificationOptionJSONObject, "Key",
				String::toLowerCase);

			_copy(jsonObject, productSpecificationOptionJSONObject, "Value");

			productSpecificationOptionJSONObjects.put(
				key, productSpecificationOptionJSONObject);

			// All

			JSONObject specificationOptionJSONObject = new JSONObject();

			_copy(
				jsonObject, specificationOptionJSONObject, "CategoryKey",
				categoryKey -> ((String)categoryKey).toLowerCase());
			_copy(
				jsonObject, specificationOptionJSONObject, "Facetable", false);

			key = _copy(
				jsonObject, specificationOptionJSONObject, "Key",
				String::toLowerCase);

			_specificationOptionJSONObjects.put(
				key, specificationOptionJSONObject);
		}

		return _getJSONArray(productSpecificationOptionJSONObjects.values());
	}

	private final Map<String, JSONObject> _optionCategoryJSONObjects =
		new TreeMap<>();
	private final Map<String, JSONObject> _optionJSONObjects = new TreeMap<>();
	private final Map<String, JSONObject> _productJSONObjects = new TreeMap<>();
	private final Map<String, JSONObject> _specificationOptionJSONObjects =
		new TreeMap<>();

}